<?php

namespace App\Api\V2\Controllers\Auth;

use App\Api\V2\Controllers\Controller;
use App\Http\Requests\LoginUserRequest;
use App\Http\Requests\StoreUserRequest;
use App\Services\CreateUserService;
use App\Transformers\UserTransformer;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Facades\JWTAuth;
use Illuminate\Http\Request;

class AuthenticateController extends Controller
{
    /**
     * @param Request $request
     * @param CreateUserService $createUser
     * @param StoreUserRequest $validateUser
     * @return mixed
     */
    public function register(Request $request, CreateUserService $createUser, StoreUserRequest $validateUser)
    {
        $credentials = $request->all();

        $newUser = $createUser->make($credentials);

        return $this->response->item($newUser, new UserTransformer);
    }

    /**
     * @param Request $request
     * @param LoginUserRequest $validateUser
     * @return mixed
     */
    public function login(Request $request, LoginUserRequest $validateUser)
    {
        $credentials = $request->all();

        try {
            if (!$token = JWTAuth::attempt($credentials)) {
                return response()->json(['error' => 'invalid_credentials'], 401);
            }
        } catch (JWTException $e) {
            return response()->json(['error' => 'could_not_create_token'], 500);
        }

        return response()->json(compact('token'));
    }
}
