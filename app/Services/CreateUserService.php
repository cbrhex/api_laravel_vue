<?php

namespace App\Services;
use App\Models\User;

/**
 * Class CreateUserService
 * @package App\Services
 */
class CreateUserService
{
    /**
     * Create user
     *
     * @param array $data
     * @return User
     */
    public function make(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
    }
}