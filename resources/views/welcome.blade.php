<!DOCTYPE html>
<html lang="en" xmlns:v-bind="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>

    <!-- Custom style -->
    <link rel="stylesheet" href="{{ elixir('css/app.css') }}">
</head>
<body>

<div class="container" id="app">

    {{-- Navbar --}}
    <navbar></navbar>

    {{-- Main content --}}
    <router-view></router-view>

</div>

<script src="{{ elixir('js/app.js') }}"></script>
</body>
</html>
