/**
 * Import available modules
 */
import VueRouter from 'vue-router';

/**
 * Import auth class
 */
import Auth from './Auth/Auth';

/**
 * Require bootstrap libs/modules
 */
require('./bootstrap');

/**
 * Make auth
 * @type {Auth}
 */
Auth.init();

/**
 * Import route list
 */
let routes = require('./routes').default;

/**
 * Init main router of app
 */
export const router = new VueRouter({
    mode: 'history',
    routes: routes
});

/**
 * Init main app
 */
const app = new Vue({
    router
}).$mount('#app');