/**
 * Resource object
 * @type {Symbol}
 */
let resource = Symbol();

/**
 * Entity model
 */
export default class Entity {

    /**
     * Constructor
     */
    constructor() {
        /**
         *  Init rest http resource
         * @type {*|Http}
         */
        this[resource] = Vue.http;
    }

    /**
     * Get query
     * @param url
     * @param options
     * @returns {*}
     */
    get(url, options = {}) {
        return (this[resource].get(url, options));
    }

    /**
     * Head query
     * @param url
     * @param options
     * @returns {*}
     */
    head(url, options = {}) {
        return (this[resource].head(url, options));
    }

    /**
     * Delete query
     * @param url
     * @param options
     * @returns {*}
     */
    delete(url, options = {}) {
        return (this[resource].delete(url, options));
    }

    /**
     * Jsonp query
     * @param url
     * @param options
     * @returns {*}
     */
    jsonp(url, options = {}) {
        return (this[resource].jsonp(url, options));
    }

    /**
     * Post query
     * @param url
     * @param body
     * @param options
     * @returns {*}
     */
    post(url, body = {}, options = {}) {
        return (this[resource].post(url, body, options));
    }

    /**
     * Put query
     * @param url
     * @param body
     * @param options
     * @returns {*|Entry|undefined}
     */
    put(url, body = {}, options = {}) {
        return (this[resource].put(url, body, options));
    }

    /**
     * Path query
     * @param url
     * @param body
     * @param options
     * @returns {*}
     */
    path(url, body = {}, options = {}) {
        return (this[resource].path(url, body, options));
    }
}