import Entity from '../Entity';

/**
 * API config
 * @type {Symbol}
 */
let apiConfig = Symbol();

/**
 * Auth config
 * @type {Symbol}
 */
let authConfig = Symbol();

/**
 * AuthRepository
 */
export default class AuthRepository extends Entity{

    /**
     * Constructor
     */
    constructor() {
        super();

        /**
         * Set API config
         */
        this[apiConfig] = require('../../Config/api').default;

        /**
         * Set Auth config
         */
        this[authConfig] = require('../../Config/auth').default;
    }

    /**
     * Sign in repo
     * @param login
     * @param pass
     * @returns {*}
     */
    signin(login, pass) {
        return this.post(
            this[apiConfig].API_HOST +
            this[apiConfig].API_PREFIX_URI +
            this[authConfig].AUTH_LOGIN_URL,
            {
                email: login,
                password: pass
            }
        );
    }

}