/**
 * Route list application
 */
export default [
    {
        /**
         * Home page route
         */
        path: '/',
        name: 'home',
        exact: true,
        component: require('./ViewModel/Home.vue')
    },
    {
        /**
         * Login page route
         */
        path: '/auth/login',
        name: 'login',
        exact: true,
        component: require('./ViewModel/Auth/Login.vue')
    },
    {
        /**
         * Register page route
         */
        path: '/auth/register',
        name: 'register',
        exact: true,
        component: {template: '<div>Register</div>'}
    },
    {
        /**
         * List of users page route
         */
        path: '/users',
        name: 'users',
        exact: true,
        component: require('./ViewModel/Users.vue')
    },
];

