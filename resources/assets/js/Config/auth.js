/**
 * Auth config
 */
export default {
    /**
     * AUTH_LOGIN_URL
     * @type {string}
     */
    AUTH_LOGIN_URL: '/login',

    /**
     * AUTH_REGISTER_URL
     * @type {string}
     */
    AUTH_REGISTER_URL: '/register'
}