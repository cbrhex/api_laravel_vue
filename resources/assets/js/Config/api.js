/**
 * API config
 */
export default {
    /**
     * API_HOST
     * @type {string}
     */
    API_HOST: 'http://apiexample',

    /**
     * API_PREFIX_URI
     * @type {string}
     */
    API_PREFIX_URI: '/api'
}