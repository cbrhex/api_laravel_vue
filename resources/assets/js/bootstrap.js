/**
 * lodash. A modern JavaScript utility library delivering modularity, performance & extras.
 */
window._ = require('lodash');

/**
 * jquery and bootstrap
 */
window.$ = window.jQuery = require('jquery');
require('bootstrap-sass');

/**
 * vue require
 */
window.Vue = require('vue');


/**
 * vue-resource require
 */
window.VueResource = require('vue-resource');
Vue.use(VueResource);

/**
 * vue route require
 */
window.VueRouter = require('vue-router');
Vue.use(VueRouter);

/**
 * register components. move to another file
 */
Vue.component('navbar', require('./ViewModel/Navbar.vue'));

/**
 * If need CSRF token
 */
// Vue.http.interceptors.push((request, next) => {
//     request.headers.set('X-CSRF-TOKEN', Laravel.csrfToken);
//
//     next();
// });