/**
 * States
 * @type {{authenticated: boolean}}
 */
let states = { authenticated: false };

/**
 * Auth
 */
export default class Auth {

    static init() {
        /**
         * Check auth state
         */
        this.checkAuth();
    }

    /**
     * setAuthenticated
     */
    static setAuthenticated() {
        states.authenticated = true;
    }

    /**
     * setUnAuthenticated
     */
    static setUnAuthenticated() {
        states.authenticated = false;
    }

    /**
     * Is authenticated
     * @returns {boolean}
     */
    static getAuthenticated() {
        return states.authenticated;
    }

    /**
     * Set token
     * @param token
     */
    static setToken(token) {
        localStorage.setItem('token', token);
    }

    /**
     * Get token
     */
    static getToken() {
        return localStorage.getItem('token');
    }

    /**
     * Remove token
     */
    static removeToken() {
        localStorage.removeItem('token');
    }

    /**
     * Get states
     * @returns {{authenticated: boolean}}
     */
    static getStates() {
        return states;
    }

    /**
     * Check auth state
     */
    static checkAuth() {
        if (this.getToken()) {
            this.setAuthenticated();
        }
        else {
            this.setUnAuthenticated();
        }
    }

    /**
     * Get auth token for request
     * @returns {{Authorization: string}}
     */
    static getAuthHeader() {
        return {
            'Authorization': 'Bearer ' + this.getToken()
        }
    }
}