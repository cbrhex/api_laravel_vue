import Auth from './Auth';
import AuthRepository from '../Entity/Auth';

/**
 * Instance
 * @type {null}
 */
let instance = null;

/**
 * Auth repo
 * @type {Auth}
 */
let authRepository = new AuthRepository();

/**
 * Login
 */
export default class Login extends Auth {

    /**
     * Constructor
     */
    constructor() {

        /**
         * Make singleton
         */
        if (!instance) {
            super();

            instance = this;
        }

        return instance;
    }

    /**
     * Sign in
     * @param login
     * @param pass
     * @param success
     */
    signin(login, pass, success) {
        authRepository.signin(login, pass).then((response) => {
            // set current state as authenticated
            Auth.setAuthenticated();

            // set current auth token
            Auth.setToken(response.body.token);

            // set callback success in true
            success({ state: true });

        }, (response) => {

            // errors array for callback
            let errors = [];

            // if we have email errors
            if (typeof response.body.errors.email !== 'undefined') {
                errors = errors.concat(response.body.errors.email);
            }

            // if we have password errors
            if (typeof response.body.errors.password !== 'undefined') {
                errors = errors.concat(response.body.errors.password);
            }

            // set callback success in false
            success({ state: false, errors: errors });
        });
    }

    /**
     * Logout
     */
    logout() {
        Auth.setUnAuthenticated();
        Auth.removeToken();
    }
}