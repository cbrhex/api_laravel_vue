<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

$api = app('Dingo\Api\Routing\Router');

// set version V2
$api->version('v2', ['middleware' => 'api', 'providers' => ['jwt']], function ($api) {

    // group for auth
    $api->group(['namespace' => 'App\Api\V2\Controllers'], function () use ($api) {

        // get test
        $api->get('get/test', [
            'uses' => function(){
                return ['status' => true];
            }]);

        // login route
        $api->post('login', [
            'uses' => 'Auth\AuthenticateController@login']);

        // register route
        $api->post('register', [
            'uses' => 'Auth\AuthenticateController@register']);
    });
});